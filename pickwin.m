function [indN,indP,indS,indC,indAll,Flag,Param] = pickwin(u,fs,Tp,Ts,Tend,M,varargin)
%   "pickwin.m" is a versatile Matlab algorithm that gives the seismic window for the P-, S-, coda and
% the full signal phase as well as a noise window for Signal-to-Noise (SNR) purpose. This function
% provides the various windows for a single earthquake recorded by one, two or the three components
% of one or several stations as long as they are sufficiently close to have nearly synchronous
% seismic wave arrivals. Giving the records for more than one station is useful only when common
% noise window is required for every stations. This work is the associate program of the paper:
% Perron et al. (2017) Selecting time windows of seismic phases and noise for engineering seismology
% applications: a versatile methodology and algorithm. Bulletin of Earthquake Engineering.
%   Three different noise windows are tested: one pre-event (N1) and two post-event windows (a short
% N2 and a long window N3). The scheme is proposed for selecting of the best noise window in terms
% of duration (as long as possible) and mean energy (as low as possible), without including
% undesirable transients. The P-wave window is defined between Tp and Ts while the S-wave duration
% is defined by a complete scheme that takes into account the expansion due to the propagation 
% (approximate by Ts-Tp) and the source (through 1/fc). The coda window is defined between
% Tc=3.3Ts-2.3Tp, that is equivalent to the Aki (1969) Tc expression, and the end of the signal
% while the full signal is defined between Tp and Tend. the rate of tapering (tx) can be specified
% in the window selection process, to enlarge the windows and apply the tapering outside the
% accurate delimitation of the phase windows.
% Warning: All the time must be given relatively to the first sample of "u" such as u(1,:)
%  correspond to the time t(1) = 0s.
%
%  [indN,indP,indS,indC,indAll,Flag,Param] = pickwin(u,fs,Tp,Ts,Tend,M,opt)
%
%   in:  u     Time series for each component store in columns. The number of components is unlimited
%              -> matrix, ex: u(samples,components)
%        fs    Sample frequency (Hz) -> scalar
%        Tp    P-wave first arrival time Tp (s) -> scalar
%   optional (but recommended):
%        Ts    S-wave first arrival time Ts (s) -> scalar
%        Tend  Time corresponding to the End of the signal (s) -> scalar
%        M     Moment Magnitude (the approximation through any other magnitude is acceptable)
%              -> scalar
%        opt   is any setting parameters that can be settled -> structure or classical inputs. ex:
%                _pickwin(...,S) with S.tx = 0.05; S.Dsmin = 3500, ...
%                _pickwin(...,'tx',0.05,'Dsmin',3500,...)
%
%   out: indN     Index of the selected noise window
%        indP     Index of the P-wave window
%        indS     Index of the S-wave window
%        indC     Index of the coda-wave window
%        indAll   Index of the full signal window
%        Flag     Such as Flag = [Noise-flag, P-flag, S-flag, coda-flag, all-flag] with P-flag,
%                 S-flag, coda-flag, and all-flag are equal to 1 if the corresponding window has
%                 been successfully evaluated and 0 otherwise. 7 values are possible for the Noise-
%                 flag:
%                       -3 -> N3 selected without the possibility to take N1
%                       -2 -> N2 selected without the possibility to take N1
%                       -1 -> N1 selected without the possibility to take N2 or N3
%                        0 -> No noise window selected
%                        1 -> N1 selected
%                        2 -> N2 selected
%                        3 -> N3 selected
%
%
%% Setting parameters
% The setting parameters give the possibility to adapt the algorithm to every dataset:
%
% %%% General setting parameters %%%
%  'tx'              Apodisation rate
%  'fsnew'           New sample frequency, only if a re-sampling is expected otherwise must be set to 0
%  'N'               Minimal number of wavelengths allowed to insure a good spectral resolution
%  'TXok'            Minimal portion (rate) of the signal that has to be ok to perform the computation
%  'TXend'           Cumulate energy rate used to define Tend when it is not provided as an input
%
% %%% S-wave setting parameters %%%
%  'Vs'              S-wave mean velocity (m/s)
%  'StressDrop'      Stress drop (bars)
%  'Fsource'         Factor that can be used to increase or decrease the S-wave duration associates to the source
%  'Fpath'           Factor that can be used to increase or decrease the S-wave duration associates to the path
%  'Dsmin'           Minimal S-wave duration (s). Useful for spectral resolution
%  'NoiseinSwin'     if "true" then the S-wave windows can exceed Tend otherwise they ends at Tend
%  'Dsmax'           Maximal S-wave duration (s). Useful for distant earthquakes especially
%
% %%% coda-wave setting parameters %%%
%  'Dcmin'           Minimal coda-wave duration (s)
%
% %%% noise window setting parameters %%%
%  'offsetTp'        Offset (s) before Tp for the end of the pre-event noise window (N1)
%  'Dnlim'           Minimal noise window duration allowed (s)
%  'Dnmin'           Minimal noise window duration targeted (s)
%  'Dnmax'           Maximal noise window duration allowed (s)
%  'CodainNoiseWin'  if "true" then N2 and N3 can start before Tend otherwise they are rejected
%  'TargetPhase'     Phase whose duration (Dt) is targeted. Can be 'P', 'S', 'coda' or 'all'
%  'F1'              Factor 1 for the energy comparison in the noise selection process
%  'F2'              Factor 2 for the energy comparison in the noise selection process
%  'F3'              Factor 3 for the energy comparison in the noise selection process
%  'F4'              Factor 4 for the energy comparison in the noise selection process
%
%
%% Property
%
% Vincent Perron 2017/04/04   @CEA/IRSN/ISTerre
%
% This program is freely distributed and cannot by used for commercial purpose. It is under the user
% responsibility to check that the results are conformed to his expectation.
%
% The last version of this program including potential updating or correction can be found at the
% following address: https://vincentperron@gitlab.com/vincentperron/pickwin.git
%
%
% #Subfunctions: {'inputCheck2_vPW.m', 'setInputParam_vPW.m', 'winValidation.m', 'signal_En_vPW.m',
% 'spectre_vPW.m', 'fillhole_vPW.m'}

%% Default setting parameters :

Param.tx = 0;
Param.fsnew = 0;
Param.N = 10;
Param.TXok = 0.7;
Param.TXend = 0.95;
Param.Vs = 3500;
Param.StressDrop = 10;
Param.Fsource = 1;
Param.Fpath = 1;
Param.Dsmin = 10; 
Param.NoiseinSwin = false;
Param.Dsmax = 120;
Param.Dcmin = 10;
Param.offsetTp = 2;
Param.Dnlim = 5;
Param.Dnmin = 10;
Param.Dnmax = 60;
Param.CodainNoiseWin = true;
Param.TargetPhase = 'S';
Param.F1 = 3;
Param.F2 = 2; 
Param.F3 = 1.5;
Param.F4 = 0.8;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Programme  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%% Input parameters checking:

%Verification of the number of input parammeters
if nargin<3
    error('Not enough input arguments. The signal "u", the sample frequency "fs", and "Tp" are requiered at least.');
end

[flag,msg] = inputCheck2_vPW(u,'N',1,2,[100 1]);
if any(~flag), error(['Signal "u" input parameter: ',msg]); end
[l,nc] = size(u);
if nc>l, u = u'; [l,nc] = size(u); end

[flag,msg] = inputCheck2_vPW(fs,'I',1,1,1,1,10,10000);
if any(~flag), error(['Sample frequency "fs" input parameter: ',msg]); end
t = (0:l-1)/fs;
%Verification of the presence of data in "u" :
isOk = abs(u)>0 & ~isnan(u);
isallOk = all(isOk,2);

[flag,msg] = inputCheck2_vPW(Tp,'N',1,1,1,1,0,t(end));
if any(~flag), error(['"Tp" input parameter: ',msg]); end
if nargin>3 && ~isempty(Ts) && Ts>0
    [flag,msg] = inputCheck2_vPW(Ts,'N',1,1,1,1,Tp,t(end));
    if any(~flag),  error(['"Ts" input parameter: ',msg]); end
else
    Ts = 0;
end
if nargin>4 && ~isempty(Tend) && Tend>0
    [flag,msg] = inputCheck2_vPW(Tend,'N',1,1,1,1,Ts,t(end));
    if any(~flag), error(['"Tend" input parameter: ',msg]); end
else
    Tend = 0;
end
if nargin>5 && ~isempty(M) && M>0 %The effect of the magnitude on Ds can be negleted below M5
    [flag,msg] = inputCheck2_vPW(M,'N',1,1,1,1,0,10);
    if any(~flag), error(['"M" input parameter: ',msg]); end
else
    M = 0;
end

%% Setting parameters checking :
%Implementation of the input setting parameters:
if nargin>6
    [Param] = setInputParam_vPW(Param,varargin);
end

[flag,msg] = inputCheck2_vPW(Param.tx,'N',1,1,1,1,0,0.5);
if any(~flag), error(['"Param.tx" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.fsnew,'N',1,1,1,1,0,fs);
if any(~flag), error(['"Param.fsnew" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.N,'I',1,1,1,1,0,100);
if any(~flag), error(['"Param.N" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.TXok,'N',1,1,1,1,0,1);
if any(~flag), error(['"Param.TXok" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.TXend,'N',1,1,1,1,0,1);
if any(~flag), error(['"Param.TXend" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Vs,'N',1,1,1,1,0,10000);
if any(~flag), error(['"Param.Vs" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.StressDrop,'N',1,1,1,1,0,500);
if any(~flag), error(['"Param.StressDrop" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Fsource,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.Fsource" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Fpath,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.Fpath" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dsmin,'N',1,1,1,1,0,100);
if any(~flag), error(['"Param.Dsmin" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dsmax,'N',1,1,1,1,0,1000);
if any(~flag), error(['"Param.Dsmax" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.NoiseinSwin,'L',1,1,1,1,0,1);
if any(~flag), error(['"Param.NoiseinSwin" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dcmin,'N',1,1,1,1,0,1000);
if any(~flag), error(['"Param.Dcmin" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.offsetTp,'N',1,1,1,1,0,50);
if any(~flag), error(['"Param.offsetTp" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dnlim,'N',1,1,1,1,0,1000);
if any(~flag), error(['"Param.Dnlim" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dnmin,'N',1,1,1,1,0,1000);
if any(~flag), error(['"Param.Dnmin" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.Dnmax,'N',1,1,1,1,0,10000);
if any(~flag), error(['"Param.Dnmax" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.CodainNoiseWin,'L',1,1,1,1,0,1);
if any(~flag), error(['"Param.CodainNoiseWin" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.TargetPhase,'S',1,1,1,4);
if all(flag) && all(~strcmpi(Param.TargetPhase,{'P','S','coda','all'})), flag = false;
    msg = 'only ''P'',''S'',''coda'', and ''all'' allowed'; end
if any(~flag), error(['"Param.TargetPhase" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.F1,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.F1" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.F2,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.F2" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.F3,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.F3" setting parameter: ',msg]); end

[flag,msg] = inputCheck2_vPW(Param.F4,'N',1,1,1,1,0.1,10);
if any(~flag), error(['"Param.F4" setting parameter: ',msg]); end


%% Estimation of the index of the different phase of the signal (P, S, Coda, All):

%Warning: The notation ~ is not accepted in all versions of Matlab. Choice of xx instead.
%#ok<*ASGLU>

%Initiation of the output parameters:
indN0 = [];
indP0 = [];
indS0 = [];
indC0 = [];
% Nfin1 = [];
% Nfin2 = [];
Flag0 = ones(1,5); %Initialisation of the Flag values

%Obtention of the index corresponding to the picking time:
[xx,indTp] = min(abs(t-Tp));
[xx,indTs] = min(abs(t-Ts));
if Tend==0
    %If no Tend is given then the "TXend" % of the cumulative energy is taken
    [xx,xx,indTend0] = signal_En_vPW(fillhole_vPW(u(indTp:end,:)),Param.TXend);
    indTend = indTp+indTend0-1;
else
    [xx,indTend] = min(abs(t-Tend));
end
%Verification of the maximal index possible:
if indTend>l, indTend = l; Flag0(4) = 0; Flag0(5) = 0; end

if Ts==0 %If "Ts" is not given
    
    %Definition of the P-waves window:
    Lp = round(length(indTp:indTend)/(1-Param.tx));
    if indTend-Lp+1>0
        indP0 = indTend-Lp+1:indTend;
    end
    %Definition of the All-waves window:
    indAll0 = indP0;
    
else  %If "Ts" is given
    
    %Definition of the P-waves window:
    Lp = round(length(indTp:indTs)/(1-Param.tx)); %La longueur de la fen�tre d'onde P fait donc Ts-Tp + l'�largissement avant li� au tx
    indP0 = indTs-Lp+1:indTs;
    if indP0(1)<=0, indP0 = 1:indP0(end); Flag0(2) = 0; end
    if indP0(end)>indTend, indP0 = indP0(1):indTend; Flag0(2) = 0; end
    
    %Definition of the S-waves window:
    Mo = 10^((M+6.03)*3/2);
    fc = 0.37*Param.Vs*(16*Param.StressDrop*10^5/(7*Mo)).^(1/3); %Corner frequency (Hz)
    Ls = round(min([Param.Dsmax*fs max([Param.Dsmin Param.Fsource/fc+Param.Fpath*(Ts-Tp)]*fs)])/(1-2*Param.tx)); %Length of the S-waves window (in number of sample)
    indS0 = round(indTs-Ls*Param.tx):round(indTs+Ls*(1-Param.tx)-1);
    if indS0(1)<=0, indS0 = 1:indS0(end); Flag0(3) = 0; end
    if Param.NoiseinSwin && indTend-indS0(1)+1<Param.Dsmin*fs/(1-2*Param.tx)
        if indS0(end)>l, indS0 = indS0(1):l; Flag0(3) = 0; end
    else
        if indS0(end)>indTend, indS0 = indS0(1):indTend; Flag0(3) = 0; end
    end
    
    %Definition of the Coda-waves window:
    indC0 = round(3.3*indTs-2.3*indTp):indTend;
    if isempty(indC0) || (indC0(end)-indC0(1))<Param.Dcmin*fs, indC0 = []; end %No Coda waves window wider enough available
    
    %Definition of the All-waves window:
    indAll0 = round(indTend-(indTend-indTp)/(1-Param.tx)):indTend;
    if indAll0(1)<=0, indAll0 = 1:indAll0(end); Flag0(5) = 0; end
    if indAll0(end)>indTend, indAll0 = indAll0(1):indTend; Flag0(5) = 0; end
end



%% Estimation of the index of the noise window:

%Determination of the length of the reference window:
if strcmp(Param.TargetPhase,'P')
    Lref = length(indP0);
elseif strcmp(Param.TargetPhase,'S')
    if Ts==0
        Lref = length(indP0);
    else
        Lref = length(indS0);
    end
elseif strcmp(Param.TargetPhase,'Coda')
    Lref = length(indC0);
elseif strcmp(Param.TargetPhase,'All')
    Lref = length(indAll0);
else
    Lref = 0;
end

%%%% Definition of the pre-event noise window:
Ndeb = 1:round(indTp-Param.offsetTp*fs);
indNdebOk = find(isallOk(Ndeb));
lNdeb = length(indNdebOk);

if lNdeb/fs<1 %If less than 1 s is available for the pre-event window
    [indN,indP,indS,indC,indAll,Flag] = winValidation(u,t,indN0,indP0,indS0,indC0,indAll0,Flag0,Param.fsnew,Param.TXok); %Verification and reshaping if necessary of the results
    return
end
Ndeb = (indNdebOk(1):indNdebOk(end))+Ndeb(1)-1;
if lNdeb>=Lref && lNdeb>=Param.Dnmin*fs  %if the pre-event noise available is longer than the reference window
    Ndeb = Ndeb(end)-min([max([Param.Dnmin*fs Lref])  Param.Dnmax*fs])+1:Ndeb(end);
end

[Ndeb0,indP,indS,indC,indAll,Flag] = winValidation(u,t,Ndeb,indP0,indS0,indC0,indAll0,Flag0,0,Param.TXok);
if isempty(Ndeb0) %The quality of the pre-event signal is not sufficient (presence of NaN or 0)
    indN = [];
    return
end
lNdeb = length(Ndeb); %Length of the pre-event window


%%%% Definition of the post-event noise windows:
lNfin1 = max([Param.Dnmin*fs lNdeb]); %Length of the short post-event window
lNfin2 = min([max([lNfin1 Lref]) Param.Dnmax*fs]); %Length of the long post-event window
indOk = find(isallOk);
Nfin1 = indOk(end)-lNfin1+1:indOk(end); %Short post-event window
Nfin2 = indOk(end)-lNfin2+1:indOk(end); %Long post-event window

%Definition of the post-event noise windows beginning limit :
if Tend>0 && Param.CodainNoiseWin==false  %Must be change to forbit any beginning of the post-event noise windows before Tend
    if ~isempty(indS0)
        Nfinlim = max([indTend indS0(end)]);
    else
        Nfinlim = indTend;
    end
elseif ~isempty(indS0)
    Nfinlim = indS0(end);
else
    Nfinlim = 1;
end

if Nfin1(1)<Nfinlim %If not short post-event window available
    if lNdeb/fs<(Param.Dnlim) %If the pre-event noise window is too short
        [indN,indP,indS,indC,indAll,Flag] = winValidation(u,t,indN0,indP0,indS0,indC0,indAll0,Param.fsnew,Param.TXok); %Verification and reshaping if necessary of the results
        return
    else
        indN0 = Ndeb; %The pre-event noise window is selected
        Flag0(1) = -1;
    end
else %If a short and maybe even a long post-event window available
    %Evaluation of the energy associate to each window:
    Nlong = 2^nextpow2(max([length(Ndeb) length(Nfin1) length(Nfin2)]));
    Edeb = zeros(floor(Nlong/2)+1,1); Efin1 = Edeb; Efin2 = Edeb;
    for k=1:nc %On the component
        [Udeb,f,fmin(1)] = spectre_vPW(u(Ndeb,k),fs,Param.tx,Nlong,Param.N);
        [Ufin1,xx,fmin(2)] = spectre_vPW(u(Nfin1,k),fs,Param.tx,Nlong,Param.N);
        [Ufin2,xx,fmin(3)] = spectre_vPW(u(Nfin2,k),fs,Param.tx,Nlong,Param.N);
        
        Edeb = Edeb+Udeb.^2;
        Efin1 = Efin1+Ufin1.^2;
        Efin2 = Efin2+Ufin2.^2;
    end
    indfok1 = find(f>=max(fmin([1 2])));
    indfok2 = find(f>=max(fmin([1 3])));
    RapE1 = mean(Efin1(indfok1))/mean(Edeb(indfok1));
    RapE2 = mean(Efin2(indfok2))/mean(Edeb(indfok2));
    
    if lNdeb/fs<(Param.Dnlim) %If the pre-event noise window is too short
        if RapE2>Param.F2 || Nfin2(1)<Nfinlim %If the level of energy of the long post-event window is too high
            if RapE1>Param.F1 %If the level of energy of the short post-event window is too high
                [indN,indP,indS,indC,indAll,Flag] = winValidation(u,t,indN0,indP0,indS0,indC0,indAll0,Flag0,Param.fsnew,Param.TXok);
                return
            else %If the level of energy of the short post-event window is correct
                indN0 = Nfin1;
                Flag0(1) = -2; %The short post-event window is selected
            end
        else %If the level of energy of the long post-event window is correct
            indN0 = Nfin2;
            if lNfin1==lNfin2 %The long and short window are identical
                Flag0(1) = -2; %The short post-event window is consider
            else
                Flag0(1) = -3; %The long post-event window is selected
            end
        end
    elseif lNdeb/fs<Param.Dnmin %If the pre-event noise window is short but long enough to be potentially selected
        if RapE2>Param.F2 || Nfin2(1)<Nfinlim %If the level of energy of the long post-event window is too high
            if RapE1>Param.F2 %If the level of energy of the short post-event window is too high
                indN0 = Ndeb; %The pre-event window is selected
                Flag0(1) = 1;
            else
                indN0 = Nfin1; %The short post-event window is selected
                Flag0(1) = 2;
            end
        else
            indN0 = Nfin2; %The long post-event window is selected
            if lNfin1==lNfin2
                Flag0(1) = 2;
            else
                Flag0(1) = 3;
            end
        end
    elseif lNdeb<Lref && RapE2<=Param.F3 && Nfin2(1)>Nfinlim %If the pre-event noise window is long enough but shorter than the reference phase window
        indN0 = Nfin2; %The long post-event window is selected
        Flag0(1) = 3;
    else %If the pre-event noise window is long enough
        if RapE2>Param.F4 %If the level of energy of the long post-event window is higher than in pre-event
            if RapE1>Param.F4 %If the level of energy of the short post-event window is higher than in pre-event
                indN0 = Ndeb;
                Flag0(1) = 1; %The pre-event window is selected
            else
                indN0 = Nfin1; %The short post-event window is selected
                Flag0(1) = 2;
            end
        else
            indN0 = Nfin2; %The long post-event window is selected
            if lNfin1==lNfin2
                Flag0(1) = 2;
            else
                Flag0(1) = 3;
            end
        end
    end
end

%Verification and reshaping if necessary of the results
[indN,indP,indS,indC,indAll,Flag] = winValidation(u,t,indN0,indP0,indS0,indC0,indAll0,Flag0,Param.fsnew,Param.TXok);
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Others functions call by pickwin.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%

function [flag,msg] = inputCheck2_vPW(val,format,ndimMin,ndimMax,dimMin,dimMax,valMin,valMax)
%"inputCheck2.m" permet la v�rification du format, des dimensions et des �ventuelles limites d'un param�tre. Il renvoie
% "0" dans "flag" si le param�tre est mal d�fini ainsi qu'un message associ�. Si le param�tre est correct alors tout
% les "flag" sont � "1" et le message est vide.
%
% [flag,msg] = inputCheck2(val,format,ndim,dimMin,dimMax,valMin,valMax)
%
%   in:  val      Param�tre � tester. Ce param�tre peut �tre sous diff�rents formats et dimensions
%        format   String indiquant par une ou plusieurs lettres les formats acceptables pour le
%                 param�tre. Ex: format = 'SC';. Les formats possibles sont :
%                       'S' -> String
%                       'N' -> Numeric
%                       'I' -> Integer (..., -2, -1, 0, 1, 2, ...)
%                       'L' -> Logical (0/1)
%                       'C' -> Cellule
%                       'c' -> Cellule contenant exclusivement des string
%    optionnel:
%        ndimMin     Nombre minimum de dimensions de "val". Mettre "0" si on ne souhaite pas exploiter ce crit�re
%        ndimMax     Nombre maximum de dimensions de "val". Mettre "0" si on ne souhaite pas exploiter ce crit�re
%        dimMin   Taille minimum pour chacune des dimensions de "val" tel-que peut �tre comparable � "size"
%        dimMax   Taille maximum pour chacune des dimensions de "val" tel-que peut �tre comparable � "size"
% � noter que si "val" est un scalaire ou un vecteur � une dimension alors les bornes de taille de celui-ci peuvent
% �tre directement indiqu�es dans dimMin et dimMax. Mettre "0" si on ne souhaite pas exploiter ces crit�res
%        valMin   Valeur minimum prise par le param�tre "val" lorsque celui-ci est au format "numeric"
%        valMax   Valeur maximum prise par le param�tre "val" lorsque celui-ci est au format "numeric"
%
%   out: flag(1)==0 -> Le format est incorrect
%        flag(2)==0 -> Le nombre de dimensions est insuffisant
%        flag(3)==0 -> Le nombre de dimensions est en exc�s
%        flag(4)==0 -> Une des dimensions est trop petite
%        flag(5)==0 -> Une des dimensions est trop grande
%        flag(6)==0 -> La valeur minimale de "val" est trop petite
%        flag(7)==0 -> La valeur maximale de "val" est trop grande
%        msg        -> Message d'erreur � indiquer si on le souhaite dans le cas ou un des flag==0
%
%Exemple d'utilisation:
% [nok,message] = inputCheck(scalaire,'N',1,1,1,1,-50,200); %Scalaire compris entre -50 et 200
% [nok,message] = inputCheck(vecteur,'N',1,1,[1 20],[1 50],-50,200); %Vecteur en ligne comprenant entre 20 et 50 nombres
%                                                                  %compris entre -50 et 200
% [nok,message] = inputCheck(matrice,'N',3,3,[3 3 3],[5 5 5],-50,200); %Matrice 3D comprenant entre 3 et 5 nombres �
%                                                                    %chaque dimension qui sont compris entre -50 et 200
% [nok,message] = inputCheck(string,'S'); %Param�tre au format string
%
%
% Vincent Perron 2017/03/29   @cea
% Commissariat � l'Energie Atomique


%% Verification of the input parameters

if nargin==0
    error('Input parameter "val" is required at least')
end
if nargin>1 && (~ischar(format) || (isempty(strfind(format,'S')) && isempty(strfind(format,'N')) &&...
        isempty(strfind(format,'I')) &&isempty(strfind(format,'L')) && ...
        isempty(strfind(format,'C')) && isempty(strfind(format,'c'))))
    error(['The parameter "format" has to be a char composed exclusivelly by of the following ',...
           'letters: ''S'', ''N'', ''L'', ''I'', ''C'' and/or ''c'''])
end
Ndim = find(size(val)>1);
if nargin<3 || isempty(ndimMin) || (isnumeric(ndimMin) && length(ndimMin)==1 && ndimMin==0)
    ndimMin = [];
elseif ~isnumeric(ndimMin) || length(ndimMin)~=1 || ndimMin<=0 || ndimMin~=fix(ndimMin)
    error('Input parameter "ndimMin" must be a scalar of natural number')
end
if nargin<4 || isempty(ndimMax) || (isnumeric(ndimMax) && length(ndimMax)==1 && ndimMax==0)
    ndimMax = [];
elseif ~isnumeric(ndimMax) || length(ndimMax)~=1 || ndimMax<=0 || ndimMax~=fix(ndimMax)
    error('Input parameter "ndimMax" must be a scalar of natural number')
elseif ~isempty(ndimMin) && any(ndimMax<ndimMin)
    error('Input parameter "ndimMin" cannot be higher than "ndimMax" (ndimMax>=ndimMin)')
end
if nargin<5 || isempty(dimMin) || (isnumeric(dimMin) && length(dimMin)==1 && dimMin==0)
    dimMin = [];
elseif ~isnumeric(dimMin) || any(dimMin<=0) || any(dimMin~=fix(dimMin)) || (length(dimMin)~=1 &&...
        length(dimMin)~=ndims(val))
    error('Input parameter "dimMin" must be a scalar or a vector of natural number with a length egal to the number of dimensions of "val"')
end
if nargin<6 || isempty(dimMax) || (isnumeric(dimMax) && length(dimMax)==1 && dimMax==0)
    dimMax = [];
elseif ~isnumeric(dimMax) || any(dimMax<=0) || any(dimMax~=fix(dimMax)) || (length(dimMax)~=1 &&...
        length(dimMax)~=ndims(val))
    error('Input parameter "dimMax" must be a scalar or a vector of natural number')
elseif ~isempty(dimMin) && (any(size(dimMax)~=size(dimMin)) || any(dimMax<dimMin))
    error('Input parameter "dimMin" and "dimMax" must have the same size and dimMax>=dimMin')
end
if nargin<7 || isempty(valMin)% || (isnumeric(valMin) && length(valMin)==1 && valMin==0)
    valMin = [];
elseif isempty(strfind(format,'N')) && isempty(strfind(format,'I')) && isempty(strfind(format,'L'))
    error('Input parameter "valMin" must be used for numeric parameters checking exclusively')
elseif ~isnumeric(valMin) || length(valMin)>1
    error('Input parameter "valMin" must be a scalar')
end
if nargin<8 || isempty(valMax)% || (isnumeric(valMax) && length(valMax)==1 && valMax==0)
    valMax = [];
elseif isempty(strfind(format,'N')) && isempty(strfind(format,'I')) && isempty(strfind(format,'L'))
    error('Input parameter "valMax" must be used for numeric parameters checking exclusively')
elseif ~isnumeric(valMax) || length(valMax)>1
    error('Input parameter "valMax" must be a scalar')
elseif ~isempty(valMin) && valMax<valMin
    error('Input parameter must be: "valMax">="valMin"')
end


%% Initiation of the output parameters
msg = '';
flag = true(1,7);

%% Program:

%Checking of the type:
if nargin>1
    flag(1) = false;
    if ~isempty(strfind(format,'S'))
        if ~isempty(val) && ischar(val)
            flag(1) = true;
        end
    end
    if ~isempty(strfind(format,'N'))
        if ~isempty(val) && isnumeric(val)
            flag(1) = true;
        end
    end
    if ~isempty(strfind(format,'I'))
        if ~isempty(val) && (isnumeric(val) && all(all(all(all(all(all(all(val==fix(val)))))))))
            flag(1) = true;
        end
    end
    if ~isempty(strfind(format,'L'))
        if ~isempty(val) && (islogical(val) || (isnumeric(val) && all(all(all(all(all(all(all(val==0 | val==1)))))))))
            flag(1) = true;
        end
    end
    if ~isempty(strfind(format,'C'))
        if iscell(val)
            flag(1) = true;
        end
    end
    if ~isempty(strfind(format,'c'))
        if iscellstr(val)
            flag(1) = true;
        end
    end
end

%Checking of the dimension:
if ~isempty(ndimMin)
    if isempty(val) || (length(val)==1 && ndimMin>1) || (length(val)>1 && length(Ndim)<ndimMin)
        flag(2) = false;
    end
end
if ~isempty(ndimMax)
    if isempty(val) || (length(val)==1 && ndimMax<1) || (length(val)>1 && length(Ndim)>ndimMax)
        flag(3) = false;
    end
end
if ~isempty(dimMin)
    if isempty(val) || (length(dimMin)==1 && dimMin>length(val)) ||...
            (length(dimMin)>1 && any(size(val)<dimMin))
        flag(4) = false;
    end
end
if ~isempty(dimMax)
    if isempty(val) || (length(dimMax)==1 && dimMax<length(val)) ||...
            (length(dimMax)>1 && any(size(val)>dimMax))
        flag(5) = false;
    end
end

%Checking of the limits:
if ~isempty(valMin)
    if isempty(val) || min(min(min(min(min(min(min(val)))))))<valMin
        flag(6) = false;
    end
end
if ~isempty(valMax)
    if isempty(val) || max(max(max(max(max(max(max(val)))))))>valMax
        flag(7) = false;
    end
end

%message:
if ~flag(1) && all(flag(2:5))
    msg = 'invalid class';
elseif any(~flag(2:5)) && all(flag([1 6 7]))
    msg = 'invalid dimensions';
elseif any(~flag(6:7)) && all(flag(1:5))
    msg = 'exceeded limits';
elseif any(~flag(1)) && any(~flag(2:5))
    msg = 'invalid class and dimensions';
elseif any(~flag(2:5)) && any(~flag(6:7)) && all(flag(1))
    msg = 'invalid dimensions and exceeded limits';
end
end

function [Param] = setInputParam_vPW(Param,inputParam)
%"setInputParam.m" permet de rentrer les valeurs des param�tres d'entr�e d'une fonction dans la
% structure contenant les valeurs par d�fault.
%
% [Param] = setInputParam(Param,inputParam)
%
%   in:  Param        Structure contenant les param�tres � mettre � jours
%        inputParam   Structure ou directement les noms et valeurs des champs � mettre � jour tel-que
%                     inputParam.champ1 = valeur1; inputParam.champ2 = valeur2; ou 
%                     inputParam = {'champ1',valeurs1,'champ2',valeurs2};
%
%   out: Param        Structure des param�tres mise � jour.
%
% Vincent Perron 2017/03/29   @cea
% Commissariat � l'Energie Atomique

if nargin~=2 || ~isstruct(Param) || (~isstruct(inputParam) && ~iscell(inputParam))
    error('Invalid input parameters');
end

Name = fieldnames(Param);

if isstruct(inputParam)
    N = fieldnames(inputParam);
    for l=1:length(N)
        ind = find(strcmpi(N{l},Name));
        if isempty(ind); error(['Unknown field: ',N{l}]); end
        for m=1:length(ind)
           Param = setfield(Param,Name{ind(m)},getfield(inputParam,N{l})); %#ok<*SFLD>
        end
    end
elseif iscell(inputParam)
    count = 1;
    while count<length(inputParam)
        if ischar(inputParam{count})
            ind = find(strcmpi(inputParam{count},Name));
            if isempty(ind); error(['Unknown field: ',inputParam{count}]); end
            for m=1:length(ind)
                Param = setfield(Param,Name{ind(m)},inputParam{count+1});
            end
            count = count+2;
        elseif isstruct(inputParam{count})
            N = fieldnames(inputParam{count});
            for l=1:length(N)
                ind = find(strcmpi(N{l},Name));
                if isempty(ind); error(['Unknown field: ',N{l}]); end
                for m=1:length(ind)
                    Param = setfield(Param,Name{ind(m)},getfield(inputParam{count},N{l}));
                end
            end
            count = count+1;
        else
            error('Fields name must be "string" argument');
        end
    end
end
end

function [indN,indP,indS,indC,indAll,Flag] = winValidation(u,t,indN,indP,indS,indC,indAll,Flag,fsnew,fracOK)
if isempty(indN) || any(sum(~isnan(u(indN,:)) & u(indN,:)~=0)<length(indN)*fracOK)
    indN = [];
    Flag(1) = 0;
end
if isempty(indP) || any(sum(~isnan(u(indP,:)) & u(indP,:)~=0)<length(indP)*fracOK)
    indP = [];
    Flag(2) = 0;
end
if isempty(indS) || any(sum(~isnan(u(indS,:)) & u(indS,:)~=0)<length(indS)*fracOK)
    indS = [];
    Flag(3) = 0;
end
if isempty(indC) || any(sum(~isnan(u(indC,:)) & u(indC,:)~=0)<length(indC)*fracOK)
    indC = [];
    Flag(4) = 0;
end
if isempty(indAll) || any(sum(~isnan(u(indAll,:)) & u(indAll,:)~=0)<length(indAll)*fracOK)
    indAll = [];
    Flag(5) = 0;
end

%Si r��chantillonnage:
if length(fsnew)==1 && fsnew>0
    tnew = (0:1/fsnew:t(end));
    if ~isempty(indN)
        [xx,indN1] = min(abs(tnew-t(indN(1))));
        [xx,indN2] = min(abs(tnew-t(indN(end))));
        indN = indN1:indN2;
    end
    if ~isempty(indP)
        [xx,indP1] = min(abs(tnew-t(indP(1))));
        [xx,indP2] = min(abs(tnew-t(indP(end))));
        indP = indP1:indP2;
    end
    if ~isempty(indS)
        [xx,indS1] = min(abs(tnew-t(indS(1))));
        [xx,indS2] = min(abs(tnew-t(indS(end))));
        indS = indS1:indS2;
    end
    if ~isempty(indC)
        [xx,indC1] = min(abs(tnew-t(indC(1))));
        [xx,indC2] = min(abs(tnew-t(indC(end))));
        indC = indC1:indC2;
    end
    if ~isempty(indP)
        [xx,indAll1] = min(abs(tnew-t(indAll(1))));
        [xx,indAll2] = min(abs(tnew-t(indAll(end))));
        indAll = indAll1:indAll2;
    end
end
end

function [U,Flag] = fillhole_vPW(u,tx,Long,Fs)
% "fillhole_vPW" est une fonction qui permet la mise en forme des donn�es avant
% calcul de fft. Les donn�es sont ainsi remises autour de 0 (retrait de la
% moyenne et detrend). Les trous ou NaN pr�sents dans les donn�es sont
% remplac�s par des 0 et les cosinus taper sont appliqu�s � chaque portion
% de signaux. Les fen�tres particuli�rement petites de trou ou de signal
% sont prises en consid�ration individuellement en fonction de ce qui les
% entoure et de leurs longueurs : Les portions courtes de trou sont remplac�es
% par un signal interpol� lin�airement. Les courtes portions de signaux sont
% soit conserv�es et apodis�es soit supprim�es lorsqu'elles sont trop courtes
% pour �tre utilisables. Cette fonction offre �galement la possibilit� de
% r��chantillonner le signal.
%
%  [U,Flag] = fillhole_vPW(u,tx,Long,Fs)
%
%En entr�e :   u       Signal � traiter de longueur L � une composante
%    optionnel:
%              tx      Taux d'apodisation (e.g. 0.05 -> 5% de L)
%              Long    Longueur de signal souhait� apr�s bourrage de 0. Si
%                      Long = 1 alors bourrage de 0 jusqu'� la prochaine
%                      puissance de 2 sup�rieure � L
%              Fs      Fr�quences d'�chantillonnage initiales et finale pour
%                      r��chantillonnage tq :  Fs = [iniFs finFs];
%
%En sortie :   U       Signal remis en forme
%              Flag    Indice des diff�rentes parties du signal tq :
%                           0 = Trous (remplissage de 0)
%                           1 = Signal original inchang� (sauf retrait de la moyenne et de la
%                               tendance)
%                           2 = Signal avec taper
%                           3 = Bouchage de trou par interpolation
%
% Vincent Perron 2015/02/26   @cea
% Commissariat � l'Energie Atomique


%% Param�tres modifiables par l'utilisateur:

% Pour d�finir le seuil en dessous duquel on interpole pour combler les trous
% mais �galement en dessous duquel on supprime le morceau de signal s'il est
% plus court que les trous qui l'entourent, un double crit�re est d�fini :
Lfrac = 0.1;      %Fraction de la longueur du signal L (e.g. 0.1 -> 10% de L)
nbrEchMax = 1000; % Nombre d'�chantillons maximum
%Le seuil sera d�finit par : nbrEch = min([Lfrac*L,nbrEchMax]);
  

%% Programme � conserver intacte
if nargin<1 || nargin>4 || length(u)<10 || all(all(isnan(u))) || all(all(u==0))
    U = []; Flag = [];
    warning('Param�tres non valables')
    return
end

format long
[nl,nc] = size(u);
if nl<nc; u = u'; end %si le signal n'est pas en colonne
L = length(u); %Nombre d'�chantillon dans le signal
nbrEch = min([Lfrac*L,nbrEchMax]);
U = zeros(size(u));% initialisation de U
Flag = zeros(size(u)); %initialisation de Flag


%Recherche des portions de signal corrompu :
isok1 = (~isnan(u) & u~=0); isok2 = isok1;
isok2(2:end-1) = u(2:end-1)==0 & u(1:end-2)~=0 & u(3:end)~=0; %on garde les 0 isol�s
indok = find(isok1 | isok2);
if isempty(indok), return; end

%replacement du signal autour de 0 :
u(indok) = u(indok)-mean(u(indok));
u(indok) = detrend(u(indok));

%Valmoy = mean(abs(u(indok)));
%Detection de tt les d�but et fin des portions de signaux :
indSigDeb = [indok(1) ; indok(find(diff(indok)>1)+1)];
indSigEnd = [indok(diff(indok)>1) ; indok(end)];
%error(' ')
Lsig = indSigEnd-indSigDeb+1;

indSigDebBis = indSigDeb;
indSigEndBis = indSigEnd;
if length(indSigDeb)>1 %Si le signal est morcel�
    %Detection de tt les d�but et fin des portions de trou :
    indTrouDeb = indSigEnd(1:end-1)+1;
    indTrouEnd = indSigDeb(2:end)-1;
    Ltrou = indTrouEnd-indTrouDeb+1;
    
    %d�termination des portions de trou � compl�ter par interpolation (ceux
    %plus petit que nbrEch et qui sont entour� de signaux plus long ou tout
    %ceux de moins de 20 �chantillons) :
    ind1 = find(Ltrou<=nbrEch);
    ind2 = find((Lsig(ind1)>=Ltrou(ind1) & Lsig(ind1+1)>=Ltrou(ind1)) | Ltrou(ind1)<=20);
    for k=length(ind2):-1:1 %Comblement du trou
        if Ltrou(ind1(ind2(k)))>1 || (Ltrou(ind1(ind2(k)))==1 && isnan(u(indTrouDeb(ind1(ind2(k)))))) %si il ne sagit d'un 0 isol� on interpole
            interp = linspace(u(indSigEnd(ind1(ind2(k)))),u(indSigDeb(ind1(ind2(k))+1)),Ltrou(ind1(ind2(k)))+2);
            u(indTrouDeb(ind1(ind2(k))):indTrouEnd(ind1(ind2(k)))) = interp(2:end-1);
            Flag(indTrouDeb(ind1(ind2(k))):indTrouEnd(ind1(ind2(k)))) = ones(Ltrou(ind1(ind2(k))),1)*3;
        end
        indSigDebBis(ind1(ind2(k))+1) = [];
        indSigEndBis(ind1(ind2(k))) = [];
    end
end
%Sauvegarde des nouvelles bornes de troncons de signaux :
indSigDeb = indSigDebBis;
indSigEnd = indSigEndBis;


nbSig = length(indSigDeb);
for k=1:nbSig %Boucle sur chaque morceau de signal
    indSig = indSigDeb(k):indSigEnd(k); %Fenetre de signal
    if k==1 %Fenetre de trou avt
        if indSigDeb(k)==1 %si le signal commence au d�but
            indEmpty1 = [];
        else %Sinon on place des 0
            indEmpty1 = 1:indSigDeb(k)-1;
            U(indEmpty1) = zeros(length(indEmpty1),1);
        end
    else
        indEmpty1 = indSigEnd(k-1)+1:indSigDeb(k)-1; %indice de la fenetre de trou avt
    end
    if k==length(indSigDeb) %Fenetre de trou apr�s
        if indSigEnd(k)==L %Si le signal finit � la fin
            indEmpty2 = [];
        else %Sinon on place des 0
            indEmpty2 = indSigEnd(k)+1:L;
            U(indEmpty2) = zeros(length(indEmpty2),1);
        end
    else
        indEmpty2 = indSigEnd(k)+1:indSigDeb(k+1)-1; %indice de la fenetre de trou apr�s
    end
    %Comparaison des longueurs de chacune des fen�tres
    L0 = length(indSig);    %Fen�tre de signal
    L1 = length(indEmpty1); %Fen�tre de trou avt
    L2 = length(indEmpty2); %Fen�tre de trou apr�s
    
    %Si moins de "nbrEch" �chantillons ds le signal et qu'il est entour� de
    %long trou on l'efface :
    if L0<nbrEch && (L1>L0 || L1==0) && (L2>L0 || L2==0)
        U(indSig) = zeros(length(indSig),1); %Remplacement du signal par des 0
    else %Sinon on apodise la fen�tre concern�e sur tx de sa longueur
        if nargin>1 && length(tx)==1 && tx>0 && tx<0.5
            [U(indSig),indtaperok] = cosine_taper2(u(indSig),tx);
            indtaperok(indtaperok==0) = 2;
        else
            U(indSig) = u(indSig);
            indtaperok = ones(size(U(indSig)));
        end
       Flag(indSig(Flag(indSig)==0)) = indtaperok(Flag(indSig)==0); %Enregistrement des indices pour lesquels le signal n'a pas �tait modifi�
    end
end

%Resample si n�cessaire :
if nargin>3 && length(Fs)==2 && all(isnumeric(Fs)) && all(Fs>0)
    U = resample(U,Fs(2),Fs(1));
    U(1) = 0; U(end) = 0;
    Flag = resample(Flag,Fs(2),Fs(1));
    U2(1:length(U)) = U;
    U = U2;
end

%Initialisation des param�tres et bourrage de 0 si n�cessaire
if nargin>=3 && length(Long)==1 && (Long==1 || Long>=length(U))
    if Long==1, Long = 2^nextpow2(L); end
    U2 = zeros(Long,1);
    Flag2 = zeros(Long,1);
    U2(1:length(U),1) = U;
    Flag2(1:length(Flag),1) = Flag;
    U = U2;
    Flag = Flag2;
end

if nl<nc; U = U'; Flag = Flag';  end
end

function [U,f,fmin] = spectre_vPW(u,Fs,tx,Long,N)
% "spectre_vPW.m" permet le calcul des spectre_vPWs de Fourier de signaux temporels.
%
%  [U,f,fmin] = spectre_vPW(u,Fs,tx,Long,N)
%
% En entr�e:
%     u      Signal d'entr�e � une composante
%     Fs     Fr�quence d'�chantillonnage ou pas d'�chantillonnage au choix
%     tx     Taux d'apodisation de la fen�tre temporelle tx = 0.05 => 5%
%     Long   Longueur de signal souhait�e apr�s bourrage de 0. La longueur du
%            spectre_vPW correspondant sera L = floor(Long/2)+1;
%            Si Long = 1 alors la longueur sera �gale � la premi�re puissance
%            de 2 sup�rieure.
%     N      Nombre minimum de longueur d'onde n�cessaire � l'estimation de 
%            chaque fr�quence (optionnel : N = 10 par d�faut, N>5 recommand�)
%
% Note : Pour sauter un param�tre d'entr�e il suffit d'attribuer � ce
% param�tre la valeur "0".
% En sortie :
%     U = spectre_vPWs en amplitude (1er �chantillon correspond � la fr�quence nulle)
%     f = fr�quences correspondantes
%     fmin = Fr�quence minimum � partir de laquelle il y a au moins N longueur d'onde.
%
% Vincent Perron 2017/03/29  @cea
% Commissariat � l'Energie Atomique

U = [];
f = [];
fmin = [];
nbpts = [];

%Lecture du param�tre Fs quelque soit le format fourni :
Fsend = Fs;
if nargin<4
    Long = [];
end
if nargin<5 || length(N)~=1 || N<1
    N = 10;
end

indSig = find(abs(u)>0 & ~isnan(u));
if isempty(indSig), return; end
Lini = indSig(end)-indSig(1)+1;
%Fr�quence minimum � partir de laquelle au moins N longueurs d'ondes sont
%concern�es par la fft :
fmin = N*Fs/Lini;

[u,flagU] = fillhole_vPW(u,tx,Long);
if isempty(u) || sum(flagU~=0)==0, return; end
Lfin = sum(flagU~=0);

Y = fft(u,nbpts,1);
L = length(Y);
Nn = floor(L/2);
%generate the symetric frequency vector (sym):
ftemp = ((0:L-1)'-Nn)/L*Fsend; 
idf = [Nn+1:L,1:Nn];
f0 = ftemp(idf);
f = abs(f0(1:Nn+1)); %r�cup�ration de la partie positive du vecteur f
U(1) = 0;
U(2:Nn+1,1) = abs(Y(2:Nn+1)) + abs(Y(end:-1:L-Nn+1));
U = U./sqrt(Lfin); %Normalisation du spectre_vPW
end

function [E,En,ind] = signal_En_vPW(u,tx)
% "signal_En_vPW" �value l'�nergie cumul�e d'un signal � n composantes rang�es
% en colonnes. Il fournit �galement l'�nergie cumul�e normalis�e et les
% indices correspondant aux taux d'�nergie cumul�e totale recherch�s (tx)
%
%
% [E,En,ind] = signal_En_vPW(u,tx)
%
%   in:  u        Signal avec composantes rang�es en colonne
%        tx       Taux de l'�nergie total recherche. Ex: tx = [0.05 0.95];
%
%   out: E        Energie cumul�e
%        E        Energie cumul�e normalis�e
%        ind      Indices correspondants aux taux recherch�s
%
% Vincent Perron 2015/11/07   @cea
% Commissariat � l'Energie Atomique

[nl,nc] = size(u);
e = zeros(nl,1);
for p=1:nc
    e = e+u(:,p).^2;
end
E = cumsum(e);
En = E/E(end);

ind = ones(size(tx))*NaN;
if nargin>1
    for j=1:length(tx)
        [xx,ind(j)] = min(abs(En-tx(j)));
    end
end
end
